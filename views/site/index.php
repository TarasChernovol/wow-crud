<?php

/* @var $this yii\web\View */

$this->title = 'My Ad Application';

use yii\grid\GridView;
use yii\helpers\Html; ?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <?php foreach ($models as $model) { ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3><?php echo htmlspecialchars($model['title']) ?></h3>
                            <p><?php echo htmlspecialchars($model['description']) ?></p>
                            <p><?php echo htmlspecialchars($model['author_name']) ?></p>

                            <?= yii\helpers\Html::a('View',
                                ['site/view', 'id' => (int)$model['id']],
                                [
                                    'class' => 'btn btn-info',
                                ]); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row"><?= yii\widgets\LinkPager::widget(['pagination' => $pages]) ?></div>
    </div>
</div>
