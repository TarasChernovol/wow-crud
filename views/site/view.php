<?php

/* @var $this yii\web\View */

$this->title = $model->title;

use yii\grid\GridView;
use yii\helpers\Html; ?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="thumbnail">
                <div class="caption">
                    <h3><?php echo htmlspecialchars($model->title) ?></h3>
                    <p><?php echo htmlspecialchars($model->description) ?></p>
                    <p><?php echo htmlspecialchars($model->author_name) ?></p>

                    <?php if (Yii::$app->user->getId() === (int)$model['user_id']) { ?>
                        <?= yii\helpers\Html::a('Delete',
                            ['ad/delete', 'id' => (int)$model['id']],
                            [
                                'class' => 'btn btn-danger',
                                'data-confirm' => 'Are you sure?',
                                'data-method' => 'post',
                            ]); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
