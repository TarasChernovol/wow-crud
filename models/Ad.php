<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ad".
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property string $author_name
 * @property string $created_at
 */
class Ad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'author_name'], 'required'],
            [['description'], 'string'],
            [['created_at'], 'safe'],
            [['title', 'author_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'author_name' => 'Author Name',
            'created_at' => 'Created At',
        ];
    }
}
